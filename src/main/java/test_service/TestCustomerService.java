/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author USER
 */
public class TestCustomerService {
    
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        Customer cus1 = new Customer("kob", "0881881888");
        cs.addNew(cus1);
        Customer edCus = cs.getByTel("0881881888");
        edCus.setTel("0881881111");
        cs.update(edCus);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        
        Customer delCus = cs.getByTel("0881881111");
        cs.delete(delCus);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
    }
}
